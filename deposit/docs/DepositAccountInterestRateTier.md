# DepositAccountInterestRateTier

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**EncodedKey** | **string** | The encoded key of the interest rate tier, auto generated, unique | [optional] [default to null]
**EndingBalance** | **float32** | The top-limit value for the account balance in order to determine if this tier is used or not | [default to null]
**InterestRate** | **float32** | The rate used for computing the interest for an account which has the balance less than the ending balance | [default to null]
**EndingDay** | **int32** | The top-limit value for the account period since activation in order to determine if this tier is used or not | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



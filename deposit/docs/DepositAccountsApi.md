# \DepositAccountsApi

All URIs are relative to *https://localhost:8889/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**ChangeState**](DepositAccountsApi.md#ChangeState) | **Post** /deposits/{depositAccountId}:changeState | Allows posting an action such as approve deposit account
[**Create**](DepositAccountsApi.md#Create) | **Post** /deposits | Creates a new deposit account
[**CreateCard**](DepositAccountsApi.md#CreateCard) | **Post** /deposits/{depositAccountId}/cards | Create and associate a new card to the provided account
[**Delete**](DepositAccountsApi.md#Delete) | **Delete** /deposits/{depositAccountId} | Delete an inactive deposit account via id or encoded key
[**DeleteCard**](DepositAccountsApi.md#DeleteCard) | **Delete** /deposits/{depositAccountId}/cards/{cardReferenceToken} | Delete a card associated to the provided account via its reference token
[**GetAll**](DepositAccountsApi.md#GetAll) | **Get** /deposits | Allows retrieval of deposit accounts using various query parameters. It&#39;s possible to look up deposits by their state, branch, centre or by a credit officer to which the deposits are assigned.
[**GetAllAuthorizationHolds**](DepositAccountsApi.md#GetAllAuthorizationHolds) | **Get** /deposits/{depositAccountId}/authorizationholds | Retrieves the authorization holds related to a deposit account, ordered from newest to oldest by creation date
[**GetAllCards**](DepositAccountsApi.md#GetAllCards) | **Get** /deposits/{depositAccountId}/cards | Allows retrieval of all cards associated with the account
[**GetById**](DepositAccountsApi.md#GetById) | **Get** /deposits/{depositAccountId} | Allows retrieval of a single deposit account via id or encoded key
[**Patch**](DepositAccountsApi.md#Patch) | **Patch** /deposits/{depositAccountId} | Partially update a deposit account
[**Update**](DepositAccountsApi.md#Update) | **Put** /deposits/{depositAccountId} | Update an existing deposit account


# **ChangeState**
> DepositAccount ChangeState(ctx, depositAccountId, body)
Allows posting an action such as approve deposit account



### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **depositAccountId** | **string**| The id or encoded key of the deposit account | 
  **body** | [**DepositAccountAction**](DepositAccountAction.md)| Allows specifying the action details for a deposit account | 

### Return type

[**DepositAccount**](DepositAccount.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/vnd.mambu.v2+json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **Create**
> DepositAccount Create(ctx, body, optional)
Creates a new deposit account



### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**DepositAccount**](DepositAccount.md)| Deposit account to be created | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**DepositAccount**](DepositAccount.md)| Deposit account to be created | 
 **idempotencyKey** | **string**| Key that can be used to support idempotency on this POST. Must be a valid UUID(version 4 is recommended) string and can only be used with the exact same request. Can be used in retry mechanisms to prevent double posting. | 

### Return type

[**DepositAccount**](DepositAccount.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/vnd.mambu.v2+json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **CreateCard**
> CreateCard(ctx, depositAccountId, body, optional)
Create and associate a new card to the provided account



### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **depositAccountId** | **string**| The id or encoded key of the deposit account | 
  **body** | [**Card**](Card.md)| Card to be created | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **depositAccountId** | **string**| The id or encoded key of the deposit account | 
 **body** | [**Card**](Card.md)| Card to be created | 
 **idempotencyKey** | **string**| Key that can be used to support idempotency on this POST. Must be a valid UUID(version 4 is recommended) string and can only be used with the exact same request. Can be used in retry mechanisms to prevent double posting. | 

### Return type

 (empty response body)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/vnd.mambu.v2+json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **Delete**
> Delete(ctx, depositAccountId)
Delete an inactive deposit account via id or encoded key



### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **depositAccountId** | **string**| The id or encoded key of the deposit account | 

### Return type

 (empty response body)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/vnd.mambu.v2+json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **DeleteCard**
> DeleteCard(ctx, depositAccountId, cardReferenceToken)
Delete a card associated to the provided account via its reference token



### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **depositAccountId** | **string**| The id or encoded key of the deposit account | 
  **cardReferenceToken** | **string**| Reference token of the card to be retrieved | 

### Return type

 (empty response body)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/vnd.mambu.v2+json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetAll**
> []DepositAccount GetAll(ctx, optional)
Allows retrieval of deposit accounts using various query parameters. It's possible to look up deposits by their state, branch, centre or by a credit officer to which the deposits are assigned.



### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **offset** | **int32**| Pagination, index to start searching at when retrieving elements, used in combination with limit to paginate results | 
 **limit** | **int32**| Pagination, the number of elements to retrieve, used in combination with offset to paginate results | 
 **paginationDetails** | **string**| Flag specifying whether the pagination should be enabled or not. Please note that by default it is disabled (OFF), in order to improve the performance of the APIs | [default to OFF]
 **detailsLevel** | **string**| The level of details to retrieve, FULL means the full details of the object will be retrieved (custom fields, address, contact info or any other related object), BASIC will return only the first level elements of the object | 
 **creditOfficerUsername** | **string**| The username of the credit officer to whom the deposit accounts are assigned to | 
 **branchId** | **string**| The id/encodedKey of the branch to which the deposit accounts are assigned to | 
 **centreId** | **string**| The id/encodedKey of the centre to which the deposit accounts are assigned to | 
 **accountState** | **string**| The state of the deposit accounts to filter on | 

### Return type

[**[]DepositAccount**](DepositAccount.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/vnd.mambu.v2+json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetAllAuthorizationHolds**
> []AuthorizationHold GetAllAuthorizationHolds(ctx, depositAccountId, optional)
Retrieves the authorization holds related to a deposit account, ordered from newest to oldest by creation date



### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **depositAccountId** | **string**| The id or encoded key of the deposit account | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **depositAccountId** | **string**| The id or encoded key of the deposit account | 
 **offset** | **int32**| Pagination, index to start searching at when retrieving elements, used in combination with limit to paginate results | 
 **limit** | **int32**| Pagination, the number of elements to retrieve, used in combination with offset to paginate results | 
 **paginationDetails** | **string**| Flag specifying whether the pagination should be enabled or not. Please note that by default it is disabled (OFF), in order to improve the performance of the APIs | [default to OFF]

### Return type

[**[]AuthorizationHold**](AuthorizationHold.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/vnd.mambu.v2+json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetAllCards**
> []Card GetAllCards(ctx, depositAccountId)
Allows retrieval of all cards associated with the account



### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **depositAccountId** | **string**| The id or encoded key of the deposit account | 

### Return type

[**[]Card**](Card.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/vnd.mambu.v2+json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetById**
> DepositAccount GetById(ctx, depositAccountId, optional)
Allows retrieval of a single deposit account via id or encoded key



### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **depositAccountId** | **string**| The id or encoded key of the deposit account | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **depositAccountId** | **string**| The id or encoded key of the deposit account | 
 **detailsLevel** | **string**| The level of details to retrieve, FULL means the full details of the object will be retrieved (custom fields, address, contact info or any other related object), BASIC will return only the first level elements of the object | 

### Return type

[**DepositAccount**](DepositAccount.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/vnd.mambu.v2+json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **Patch**
> Patch(ctx, depositAccountId, body)
Partially update a deposit account



### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **depositAccountId** | **string**| The id or encoded key of the deposit account | 
  **body** | [**[]PatchOperation**](PatchOperation.md)| Patch operations to be applied to a resource | 

### Return type

 (empty response body)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/vnd.mambu.v2+json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **Update**
> DepositAccount Update(ctx, depositAccountId, optional)
Update an existing deposit account



### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **depositAccountId** | **string**| The id or encoded key of the deposit account | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **depositAccountId** | **string**| The id or encoded key of the deposit account | 
 **body** | [**DepositAccount**](DepositAccount.md)|  | 

### Return type

[**DepositAccount**](DepositAccount.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/vnd.mambu.v2+json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


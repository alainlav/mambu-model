# DepositAccountInterestRateSettings

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**EncodedKey** | **string** | The encoded for this set of interest settings, auto generated, unique | [optional] [default to null]
**InterestRate** | **float32** | The interest rate for the deposit account | [optional] [default to null]
**InterestChargeFrequency** | **string** | The interest change frequency. Holds the possible values for how often is interest charged on loan or deposit accounts | [optional] [default to null]
**InterestChargeFrequencyCount** | **int32** | The count of units to apply over the interval (e.g. [x] weeks) | [optional] [default to null]
**InterestRateTiers** | [**[]DepositAccountInterestRateTier**](DepositAccountInterestRateTier.md) | The list of interest rate tiers. An interest rate tier holds the values to define how the interest is computed | [optional] [default to null]
**InterestRateTerms** | **string** | How is the interest rate determined when being accrued for an account | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



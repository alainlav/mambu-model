# DepositAccountOverdraftSettings

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**AllowOverdraft** | **bool** | Whether this account supports overdraft or not | [optional] [default to null]
**OverdraftLimit** | **float32** | How much may be taken out as overdraft, null means no limit | [optional] [default to null]
**OverdraftExpiryDate** | [**time.Time**](time.Time.md) | The expiration date of an overdraft | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



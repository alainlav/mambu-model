# DepositAccountOverdraftBalances

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**OverdraftAmount** | **float32** | How much money has been taken out in overdraft | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



# DepositAccountInterestSettings

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**InterestRateSettings** | [***DepositAccountInterestRateSettings**](DepositAccountInterestRateSettings.md) | Interest rate settings for deposit accounts | [optional] [default to null]
**InterestPaymentSettings** | [***DepositAccountInterestPaymentSettings**](DepositAccountInterestPaymentSettings.md) | Interest payment settings for the account | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



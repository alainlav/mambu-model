/*
 * deposits
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * API version: v2
 * Generated by: Swagger Codegen (https://github.com/swagger-api/swagger-codegen.git)
 */

package swagger

// Allows specifying the action details for a deposit account
type DepositAccountAction struct {

	// The action type to be applied
	Action string `json:"action"`

	// Details for the action to be applied
	ActionDetails *DepositActionDetails `json:"actionDetails,omitempty"`
}

# Client

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**LastName** | **string** | The last name, surname or family name of the client | [default to null]
**MigrationEventKey** | **string** | The migration event encoded key associated with this client. | [optional] [default to null]
**PreferredLanguage** | **string** | The client&#39;s language of use in Mambu | [optional] [default to null]
**Addresses** | [**[]Address**](Address.md) | The addresses associated with this client information like street, city etc. | [optional] [default to null]
**Notes** | **string** | Extra notes about this client | [optional] [default to null]
**Gender** | **string** | Gender of the person, male or female | [optional] [default to null]
**GroupLoanCycle** | **int32** | Number of paid and closed (with &#39;obligations met&#39;) accounts for this client&#39;s group, when the closing operation is reverted, this is reduced | [optional] [default to null]
**PortalSettings** | [***PortalSettings**](PortalSettings.md) | The portal settings for this client | [optional] [default to null]
**AssignedBranchKey** | **string** | Encoded key of the branch this client is assigned to | [optional] [default to null]
**LoanCycle** | **int32** | Number of paid and closed (with &#39;obligations met&#39;) accounts for this client, when the closing operation is reverted, this is reduced | [optional] [default to null]
**EmailAddress** | **string** | The client&#39;s email address | [optional] [default to null]
**EncodedKey** | **string** | The encoded key of the client, auto generated, unique | [optional] [default to null]
**Id** | **string** | The id of the client, can be generated and customized, unique | [optional] [default to null]
**State** | **string** | The state of a client shows his workflow status, if he is waiting approval or is rejected or blacklisted | [optional] [default to null]
**AssignedUserKey** | **string** | Encoded key of the user this client is assigned to | [optional] [default to null]
**ClientRoleKey** | **string** | A role which describes the intended use of a client in the system | [optional] [default to null]
**LastModifiedDate** | [**time.Time**](time.Time.md) | The last date this client was modified | [optional] [default to null]
**HomePhone** | **string** | The client&#39;s home phone number | [optional] [default to null]
**CreationDate** | [**time.Time**](time.Time.md) | The date this client was created | [optional] [default to null]
**BirthDate** | **string** | The date when this client was born | [optional] [default to null]
**AssignedCentreKey** | **string** | Encoded key of the centre this client is assigned to | [optional] [default to null]
**ApprovedDate** | [**time.Time**](time.Time.md) | date when client was approved | [optional] [default to null]
**FirstName** | **string** | The first name, personal name, given name or forename of the client | [default to null]
**IdDocuments** | [**[]IdentificationDocument**](IdentificationDocument.md) | The identification documents of this person | [optional] [default to null]
**ProfilePictureKey** | **string** | Encoded key of this clients profile picture | [optional] [default to null]
**ProfileSignatureKey** | **string** | Encoded key of the users profile signature | [optional] [default to null]
**MobilePhone** | **string** | The client&#39;s mobile phone number | [optional] [default to null]
**ClosedDate** | [**time.Time**](time.Time.md) | date when client was closed | [optional] [default to null]
**MiddleName** | **string** | The middle name of the client, if she/he has one | [optional] [default to null]
**ActivationDate** | [**time.Time**](time.Time.md) | The date when client was set as active for the first time | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



# Address

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Country** | **string** | The country | [optional] [default to null]
**ParentKey** | **string** | Address parent key, the object owning this address, client, centre, branch | [optional] [default to null]
**City** | **string** | The city for this address | [optional] [default to null]
**Latitude** | **float32** | The GPS latitude of this address in signed degrees format (DDD.dddd) with 6 decimal positions, ranging from -90 to +90 | [optional] [default to null]
**Postcode** | **string** | The post code | [optional] [default to null]
**IndexInList** | **int32** | Index of this address in the list of addresses | [optional] [default to null]
**EncodedKey** | **string** | Address encoded key, unique, generated | [optional] [default to null]
**Region** | **string** | The region that is part of the address | [optional] [default to null]
**Line2** | **string** | The second line for the address, in case the first one doesn&#39;t fit the information, this is completely optional | [optional] [default to null]
**Line1** | **string** | The first line of the address | [optional] [default to null]
**Longitude** | **float32** | The GPS longitude of this address in signed degrees format (DDD.dddd) with 6 decimal positions, ranging from -180 to +180 | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



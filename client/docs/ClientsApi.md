# \ClientsApi

All URIs are relative to *https://localhost:8889/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**Create**](ClientsApi.md#Create) | **Post** /clients | Create a new client
[**Delete**](ClientsApi.md#Delete) | **Delete** /clients/{clientId} | Delete a client
[**GetAll**](ClientsApi.md#GetAll) | **Get** /clients | Allows retrieval of clients using various query parameters
[**GetById**](ClientsApi.md#GetById) | **Get** /clients/{clientId} | Allows retrieval of a single client via id or encoded key
[**Patch**](ClientsApi.md#Patch) | **Patch** /clients/{clientId} | Partially update an existing client
[**Update**](ClientsApi.md#Update) | **Put** /clients/{clientId} | Update an existing client


# **Create**
> Client Create(ctx, body, optional)
Create a new client



### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**Client**](Client.md)| Client to be created | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Client**](Client.md)| Client to be created | 
 **idempotencyKey** | **string**| Key that can be used to support idempotency on this POST. Must be a valid UUID(version 4 is recommended) string and can only be used with the exact same request. Can be used in retry mechanisms to prevent double posting. | 

### Return type

[**Client**](Client.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/vnd.mambu.v2+json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **Delete**
> Delete(ctx, clientId)
Delete a client



### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **clientId** | **string**| The id or encoded key of the client to be deleted | 

### Return type

 (empty response body)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/vnd.mambu.v2+json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetAll**
> []Client GetAll(ctx, optional)
Allows retrieval of clients using various query parameters



### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **offset** | **int32**| Pagination, index to start searching at when retrieving elements, used in combination with limit to paginate results | 
 **limit** | **int32**| Pagination, the number of elements to retrieve, used in combination with offset to paginate results | 
 **paginationDetails** | **string**| Flag specifying whether the pagination should be enabled or not. Please note that by default it is disabled (OFF), in order to improve the performance of the APIs | [default to OFF]
 **detailsLevel** | **string**| The level of details to retrieve, FULL means the full details of the object will be retrieved (custom fields, address, contact info or any other related object), BASIC will return only the first level elements of the object | 
 **firstName** | **string**| The first name, personal name, given name or forename of the client | 
 **lastName** | **string**| The last name, surname or family name of the client | 
 **idNumber** | **string**| The id number of the client&#39;s identification document | 
 **branchId** | **string**| The branch id/key to search for | 
 **centreId** | **string**| The centre id/key to search for | 
 **creditOfficerUsername** | **string**| The user name of the credit officer | 
 **state** | **string**| The state of the client to search for | 
 **birthDate** | **string**| The birth date of the client to search for | 

### Return type

[**[]Client**](Client.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/vnd.mambu.v2+json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetById**
> Client GetById(ctx, clientId, optional)
Allows retrieval of a single client via id or encoded key



### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **clientId** | **string**| The id or encoded key of the client to be retrieved | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clientId** | **string**| The id or encoded key of the client to be retrieved | 
 **detailsLevel** | **string**| The level of details to retrieve, FULL means the full details of the object will be retrieved (custom fields, address, contact info or any other related object), BASIC will return only the first level elements of the object | 

### Return type

[**Client**](Client.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/vnd.mambu.v2+json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **Patch**
> Patch(ctx, clientId, body)
Partially update an existing client



### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **clientId** | **string**| The id or encoded key of the client to be updated | 
  **body** | [**[]PatchOperation**](PatchOperation.md)| Patch operations to be applied to a resource | 

### Return type

 (empty response body)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/vnd.mambu.v2+json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **Update**
> Client Update(ctx, clientId, body)
Update an existing client



### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **clientId** | **string**| The id or encoded key of the client to be updated | 
  **body** | [**Client**](Client.md)| Client to be updated | 

### Return type

[**Client**](Client.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/vnd.mambu.v2+json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


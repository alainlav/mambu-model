# PortalSettings

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**EncodedKey** | **string** | The encoded key of the entity, generated by JDO, globally unique | [optional] [default to null]
**PortalState** | **string** | State of the client&#39;s portal preferences | [optional] [default to null]
**LastLoggedInDate** | [**time.Time**](time.Time.md) | The last date the client logged in to the portal | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



# PatchOperation

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Op** | **string** | The change to perform | [default to null]
**Path** | **string** | The field to perform the operation on | [default to null]
**From** | **string** | The field from where a value should be moved, when using move | [optional] [default to null]
**Value** | [***interface{}**](interface{}.md) | The value of the field, can be null | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



# IdentificationDocument

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**IdentificationDocumentTemplateKey** | **string** | Encoded key of the template used for this document | [optional] [default to null]
**IssuingAuthority** | **string** | Authority that issued the document, eg. Police | [optional] [default to null]
**ClientKey** | **string** | The encoded key of the client that owns this document | [optional] [default to null]
**DocumentType** | **string** | The type of the document, Passport, Id card Drivers license, etc. | [default to null]
**IndexInList** | **int32** | This document&#39;s index in the list of documents | [optional] [default to null]
**ValidUntil** | **string** | Date when the validity of the document ends | [optional] [default to null]
**EncodedKey** | **string** | The encoded key of the document, generated, unique | [optional] [default to null]
**DocumentId** | **string** | The id of the document | [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



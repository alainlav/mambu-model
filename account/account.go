package swagger

type CurrencyCode struct {
	Code string `json:"code"`
}

type SavingsAccount struct {
	AccountHolderKey  string       `json:"accountHolderKey"`  // "8a8186c3644d37760164566631a50f7d"
	AccountHolderType string       `json:"accountHolderType"` // "CLIENT"
	ProductTypeKey    string       `json:"productTypeKey"`    // "8a8186576448c7a301644b86f77d2120"
	AccountType       string       `json:"accountType"`       // "REGULAR_SAVINGS"
	AccountState      string       `json:"accountState"`      // "PENDING_APPROVAL"
	Currency          CurrencyCode `json:"currency"`          //  { "code": "CAD" }
	AssignedBranchKey string       `json:"assignedBranchKey"` // "8a818f8764263c5a016427b7394e3686"
	AssignedCentreKey string       `json:"assignedCentreKey"` // "8a818f8764263c5a016427b743513697"
}

type Account struct {
	SavingsAccount SavingsAccount `json:"savingsAccount"`
}

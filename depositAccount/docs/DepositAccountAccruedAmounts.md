# DepositAccountAccruedAmounts

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**InterestAccrued** | **float32** | The amount of interest that has been accrued in the account | [optional] [default to null]
**OverdraftInterestAccrued** | **float32** | The amount of overdraft interest that has been accrued in the account | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



# DepositAccountOverdraftInterestSettings

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**InterestRateSettings** | [***DepositAccountOverdraftInterestRateSettings**](DepositAccountOverdraftInterestRateSettings.md) | Overdraft interest rate settings for deposit accounts | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



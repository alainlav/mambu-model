# AuthorizationHold

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**EncodedKey** | **string** | The internal ID of the authorization hold, auto generated, unique. | [optional] [default to null]
**CardToken** | **string** | The reference token of the card. | [optional] [default to null]
**ExternalReferenceId** | **string** | The external reference ID to be used to reference the account hold in subsequent requests. | [default to null]
**Advice** | **bool** | Whether the given request should be accepted without balance validations. | [default to null]
**Amount** | **float32** | The amount of money to be held as a result of the authorization hold request. | [default to null]
**CurrencyCode** | **string** | The ISO currency code in which the hold was created. The amounts are stored in the base currency, but the user could have enter it in a foreign currency. | [optional] [default to null]
**CardAcceptor** | [***CardAcceptor**](CardAcceptor.md) | The card acceptor details. | [optional] [default to null]
**UserTransactionTime** | **string** | The formatted time at which the user made this authorization hold. | [optional] [default to null]
**Status** | **string** | The authorization hold status. | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



# DepositAccountInternalControls

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**RecommendedDepositAmount** | **float32** | Recommended amount for a deposit | [optional] [default to null]
**MaxWithdrawalAmount** | **float32** | The maximum amount allowed for a withdrawal | [optional] [default to null]
**TargetAmount** | **float32** | The target amount (the monthly/weekly/daily deposits should/may lead towards a savings goal) | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



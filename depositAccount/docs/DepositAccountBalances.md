# DepositAccountBalances

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**TotalBalance** | **float32** | The current balance of the account | [optional] [default to null]
**LockedBalance** | **float32** | No operation can modify the balance of the account and get it lower than this locked balance | [optional] [default to null]
**AvailableBalance** | **float32** | The current available balance for deposit transactions | [optional] [default to null]
**InterestDue** | **float32** | How much interest is due to be paid on this account | [optional] [default to null]
**FeesDue** | **float32** | How much fees is due to be paid on this account | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



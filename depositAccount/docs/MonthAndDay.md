# MonthAndDay

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Month** | **int32** | The month of the year | [optional] [default to null]
**Day** | **int32** | The day in the month | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



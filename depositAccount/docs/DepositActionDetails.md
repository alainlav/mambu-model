# DepositActionDetails

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Notes** | **string** | The notes for the action performed on the deposit account | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



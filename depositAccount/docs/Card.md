# Card

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ReferenceToken** | **string** | The reference token of the card | [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



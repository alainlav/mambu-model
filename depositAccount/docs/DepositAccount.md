# DepositAccount

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**EncodedKey** | **string** | The encoded key of the deposit account, auto generated, unique | [optional] [default to null]
**CreationDate** | [**time.Time**](time.Time.md) | The date this deposit account was created (as UTC) | [optional] [default to null]
**LastModifiedDate** | [**time.Time**](time.Time.md) | The last date the deposit account was updated (as UTC) | [optional] [default to null]
**Id** | **string** | The id of the deposit account, can be generated and customized, unique | [optional] [default to null]
**Name** | **string** | The name of the deposit account | [default to null]
**Notes** | **string** | Extra notes about this deposit account | [optional] [default to null]
**AccountHolderType** | **string** | The type of the account holder (i.e CLIENT or GROUP) | [default to null]
**AccountHolderKey** | **string** | The encodedKey of the client or group (a.k.a account holder) | [default to null]
**AccountState** | **string** | The state of the deposit account | [optional] [default to null]
**ProductTypeKey** | **string** | The key to the type of product that this account is based on | [default to null]
**AccountType** | **string** | Indicates the type of the deposit account and the product that it belongs to | [optional] [default to null]
**CreditArrangementKey** | **string** | The key to the credit arrangement where this account is registered to | [optional] [default to null]
**ApprovedDate** | [**time.Time**](time.Time.md) | The date this deposit account was approved (as Organization Time) | [optional] [default to null]
**ActivationDate** | [**time.Time**](time.Time.md) | The date this deposit account was activated (as Organization Time) | [optional] [default to null]
**LockedDate** | [**time.Time**](time.Time.md) | The date this deposit account was locked (as Organization Time) | [optional] [default to null]
**MaturityDate** | [**time.Time**](time.Time.md) | For fixed or compulsory savings plans, this is when the account matures (as Organization Time) | [optional] [default to null]
**ClosedDate** | [**time.Time**](time.Time.md) | The date this deposit account was closed (as UTC) | [optional] [default to null]
**LastInterestCalculationDate** | [**time.Time**](time.Time.md) | When/if the account had the interest last calculated (as Organization Time) | [optional] [default to null]
**LastInterestStoredDate** | [**time.Time**](time.Time.md) | When/if the account had last interest applied (stored to balance) (as Organization Time) | [optional] [default to null]
**LastOverdraftInterestReviewDate** | [**time.Time**](time.Time.md) | When the overdraft interest was last time reviewed | [optional] [default to null]
**LastAccountAppraisalDate** | [**time.Time**](time.Time.md) | When/if the account had last been evaluated for interest calculations/maturity (as Organization Time) | [optional] [default to null]
**LastSetToArrearsDate** | [**time.Time**](time.Time.md) | Date when the deposit account was set to In Arrears state, or null if the account is not In Arrears state (as Organization Time) | [optional] [default to null]
**CurrencyCode** | **string** | The currency code | [optional] [default to null]
**AssignedBranchKey** | **string** | Key of the branch this deposit account is assigned to | [optional] [default to null]
**AssignedCentreKey** | **string** | Key of the centre this account is assigned to | [optional] [default to null]
**AssignedUserKey** | **string** | Key of the user this deposit is assigned to | [optional] [default to null]
**MigrationEventKey** | **string** | The migration event encoded key associated with this deposit account. If this account was imported, track which &#39;migration event&#39; they came from. | [optional] [default to null]
**WithholdingTaxSourceKey** | **string** | The tax source from where the account withholding taxes will be updated | [optional] [default to null]
**InternalControls** | [***DepositAccountInternalControls**](DepositAccountInternalControls.md) | Groups all fields related to internal controls | [optional] [default to null]
**OverdraftSettings** | [***DepositAccountOverdraftSettings**](DepositAccountOverdraftSettings.md) | Groups all fields related to overdraft settings | [optional] [default to null]
**InterestSettings** | [***DepositAccountInterestSettings**](DepositAccountInterestSettings.md) | Groups all fields related to interest settings | [optional] [default to null]
**OverdraftInterestSettings** | [***DepositAccountOverdraftInterestSettings**](DepositAccountOverdraftInterestSettings.md) | Groups all fields related to overdraft interest settings | [optional] [default to null]
**Balances** | [***DepositAccountBalances**](DepositAccountBalances.md) | Groups all fields related to a deposit account&#39;s balances | [optional] [default to null]
**OverdraftBalances** | [***DepositAccountOverdraftBalances**](DepositAccountOverdraftBalances.md) | Groups all fields related to a deposit account&#39;s overdraft balances | [optional] [default to null]
**AccruedAmounts** | [***DepositAccountAccruedAmounts**](DepositAccountAccruedAmounts.md) | Groups all fields related to a deposit account&#39;s accrued amounts | [optional] [default to null]
**LinkedSettlementAccountKeys** | **[]string** | Lists all loan&#39;s keys on which the deposit is used as a settlement account. | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



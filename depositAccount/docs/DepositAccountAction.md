# DepositAccountAction

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Action** | **string** | The action type to be applied | [default to null]
**ActionDetails** | [***DepositActionDetails**](DepositActionDetails.md) | Details for the action to be applied | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



# DepositAccountInterestPaymentSettings

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**InterestPaymentPoint** | **string** | The interest payment point, specifies when the interest should be paid to the account | [optional] [default to null]
**InterestPaymentDates** | [**[]MonthAndDay**](MonthAndDay.md) | The list of all dates on which the interest is payed into deposit account | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



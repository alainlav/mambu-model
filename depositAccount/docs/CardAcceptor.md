# CardAcceptor

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**EncodedKey** | **string** | The internal ID of the card acceptor, auto generated, unique. | [optional] [default to null]
**Mcc** | **int32** | The Merchant Category Code of the card acceptor. | [optional] [default to null]
**Name** | **string** | The name of the card acceptor. | [optional] [default to null]
**City** | **string** | The city in which the card acceptor has the business. | [optional] [default to null]
**State** | **string** | The state in which the card acceptor has the business. | [optional] [default to null]
**Zip** | **string** | The ZIP code of the location in which the card acceptor has the business. | [optional] [default to null]
**Country** | **string** | The country in which the card acceptor has the business. | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


